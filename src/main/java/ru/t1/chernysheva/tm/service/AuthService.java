package ru.t1.chernysheva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.api.service.IAuthService;
import ru.t1.chernysheva.tm.api.service.IPropertyService;
import ru.t1.chernysheva.tm.api.service.IUserService;
import ru.t1.chernysheva.tm.exception.system.AuthenticationException;
import ru.t1.chernysheva.tm.enumerated.Role;
import ru.t1.chernysheva.tm.exception.field.LoginEmptyException;
import ru.t1.chernysheva.tm.exception.field.PasswordEmptyException;
import ru.t1.chernysheva.tm.exception.system.AccessDeniedException;
import ru.t1.chernysheva.tm.model.User;
import ru.t1.chernysheva.tm.util.HashUtil;

import java.util.Arrays;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private final IPropertyService propertyService;

    private String userId;

    public AuthService(final IPropertyService propertyService, final IUserService userService) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Nullable
    @Override
    public User registry(@Nullable final String login, @Nullable final String password) {
        return userService.create(login, password);
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AuthenticationException();
        final boolean locked = user.getLocked();
        if (locked) throw new AuthenticationException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AuthenticationException();
        if (!hash.equals(user.getPasswordHash())) throw new AuthenticationException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @NotNull
    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @NotNull
    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

    @Override
    public void checkRoles(final Role[] roles) {
        if (roles == null) return;
        @Nullable final User user = getUser();
        @Nullable final Role role = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new AccessDeniedException();
    }

}
